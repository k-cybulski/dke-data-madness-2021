"""
This module contains functions and classes for constructing conformal
predictors on top of sklearn regressors.
"""

import numpy as np
from sklearn.ensemble import GradientBoostingRegressor

def find_residual_buffer(model, significance, x_cal, y_cal):
    # Significance is (1 - coverage)
    y_pred = model.predict(x_cal).reshape((-1, 1))
    residuals = np.abs(y_pred - y_cal.reshape((-1, 1)))
    residuals = np.vstack((residuals, [np.inf]))
    return np.quantile(residuals, 1 - significance)

class ConformalResidualRegressor:
    """A class which implements an inductive conformal regressor on top of a
    scikit-learn regression model. It uses residuals as the nonconformity
    score.

    Upon prediction, this model outputs a predictive set rather than a single
    value. This set is guaranteed to cover a desired fraction of unseen
    instances from target data, assuming it's exchangeable with the calibration
    set.[1]

    [1]H. Papadopoulos, K. Proedrou, V. Vovk, and A. Gammerman, “Inductive
    Confidence Machines for Regression,” in Machine Learning: ECML 2002,
    Berlin, Heidelberg, 2002, pp. 345–356, doi: 10.1007/3-540-36755-1_29.
    """
    def __init__(self, model, significance):
        self._model = model
        self._significance = significance
        self._res_buf = None

    def fit(self, x_proper_train, y_proper_train, x_calibration, y_calibration):
        self._model.fit(x_proper_train, y_proper_train)
        self._res_buf = find_residual_buffer(self._model, self._significance, x_calibration, y_calibration)

    def predict(self, x):
        if self._res_buf is None:
            # should raise sth more fitting than value error, but whatever
            raise ValueError("Need to first fit the model")
        # Need to reshape for consistent outputs
        y_pred = self._model.predict(x).reshape((-1, 1))
        lower = y_pred - self._res_buf
        upper = y_pred + self._res_buf
        return lower, upper

def cqr_nonconformity(x, y, m_lo, m_up):
    lo = m_lo.predict(x).reshape((-1, 1))
    up = m_up.predict(x).reshape((-1, 1))
    y_rshp = y.reshape((-1, 1))
    return np.max((lo - y_rshp, y_rshp - up), axis=0)

def find_cqr_q(m_lo, m_up, significance, x_calibration, y_calibration):
    nonconformity = cqr_nonconformity(x_calibration, y_calibration, m_lo, m_up)
    nonconformity = np.vstack((nonconformity, [np.inf]))
    return np.quantile(nonconformity, 1 - significance)

class ConformalizedQuantileRegressor:
    """
    This class implements Conformalized Quantile Regression on top of gradient
    boosting. In contrast to the ConformalResidualRegressor, its predictive
    intervals are adaptive to heteroskedasticity in the data.

    [1]Y. Romano, E. Patterson, and E. Candes, “Conformalized Quantile
    Regression,” in Advances in Neural Information Processing Systems 32, H.
    Wallach, H. Larochelle, A. Beygelzimer, F. d\textquotesingle Alché-Buc, E.
    Fox, and R. Garnett, Eds. Curran Associates, Inc., 2019, pp. 3543–3553.
    """
    def __init__(self, significance):
        self._model_upper = GradientBoostingRegressor(loss='quantile', alpha=1 - significance/2)
        self._model_lower = GradientBoostingRegressor(loss='quantile', alpha=significance/2)
        self._significance = significance
        self._q = None
    
    def fit(self, x_proper_train, y_proper_train, x_calibration, y_calibration):
        self._model_upper.fit(x_proper_train, y_proper_train)
        self._model_lower.fit(x_proper_train, y_proper_train)
        self._q = find_cqr_q(self._model_lower, self._model_upper, self._significance, x_calibration, y_calibration)
    
    def predict(self, x):
        if self._q is None:
            # should raise sth more fitting than value error, but whatever
            raise ValueError("Need to first fit the model")
        # Need to reshape for consistent outputs
        y_pred_lo = self._model_lower.predict(x).reshape((-1, 1))
        y_pred_up = self._model_upper.predict(x).reshape((-1, 1))
        lower = y_pred_lo - self._q
        upper = y_pred_up + self._q
        return lower, upper
