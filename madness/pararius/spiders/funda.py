import scrapy
import time
import json
import random
from scrapy import signals
from pydispatch import dispatcher 

BASE_URL = 'https://www.funda.nl/'

class FundaSpider(scrapy.Spider):
    name = "funda"
    start_urls = ['https://www.funda.nl/huur/gemeente-maastricht/']
    index = 0
    results = {}

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def parse(self, response):
        path = r"ol.search-results > li > div > div > div > div > div > a:nth-child(1)::attr('href')"
        for item in response.css(path):
            rel_addr = item.get()
            fullitem = BASE_URL + rel_addr
            # don't waste time on huurcomplexes
            if 'huurcomplex' in rel_addr:
                print("Skipping {} from {}".format(fullitem, response.request.url))
                continue
            time.sleep(abs(random.normalvariate(2, 1)))
            yield scrapy.Request(url=fullitem, callback=self.parsePage)
        pagination_path = "nav.pagination > a:last-child::attr('href')"
        pagination_item = response.css(pagination_path).get()
        if pagination_item is not None:
            time.sleep(3)
            print(BASE_URL+pagination_item)
            yield scrapy.Request(url=BASE_URL + pagination_item, callback=self.parse)

    def parsePage(self, response):
        # Check if it's a huurcomplex, in which case just skip it
        title = response.xpath('/html/head/title/text()').get()
        if 'huurcomplex' in title.lower():
            return
        
        name = response.css(".object-header__title::text").get()
        address = response.css(".object-header__subtitle::text").get()
        area = response.xpath("//*[@title='wonen']/following-sibling::span/text()").get()
        type_of_house = response.xpath("//*[contains(text(), 'Soort appartement')]/following-sibling::dd/span/text()").get()
        if type_of_house is None:
            type_of_house = response.xpath("//*[contains(text(), 'Soort woonhuis')]/following-sibling::dd/span/text()").get()
        year_of_construction = response.xpath("//*[contains(text(), 'Bouwjaar')]/following-sibling::dd/span/text()").get()
        if year_of_construction is None:
            year_of_construction = response.xpath("//*[contains(text(), 'Bouwperiode')]/following-sibling::dd/span/text()").get()
        nr_of_rooms = response.xpath("//*[contains(text(), 'Aantal kamers')]/following-sibling::dd/span/text()").get()
        nr_bathrooms = response.xpath("//*[contains(text(), 'Aantal badkamers')]/following-sibling::dd/span/text()").get()
        nr_bedrooms = response.xpath("//*[@title='slaapkamer']/following-sibling::span/text()").get()
        if nr_bedrooms is None:
            nr_bedrooms = response.xpath("//*[@title='slaapkamers']/following-sibling::span/text()").get()
        price = response.css(".object-header__price::text").get()

        result_dict = {
        "URL": response.request.url,
        "Name": name, 
        "Price": price,
        "Address": address,
        "Area (Square metre)": area,
        "Type": type_of_house,
        "Number of Rooms": nr_of_rooms,
        "Year of Construction": year_of_construction,
        "Number of Bedrooms": nr_bedrooms,
        "Number of Bathrooms": nr_bathrooms
        }
        # Try to clean the strings a tiny bit
        for key, value in result_dict.items():
            try:
                result_dict[key] = value.strip()
            except:
                pass
        self.results[self.index] = result_dict
        self.index += 1

        return result_dict

    def spider_closed(self, spider):
        with open('funda.json', 'w') as fp:
            json.dump(self.results, fp)

