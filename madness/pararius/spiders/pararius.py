import scrapy
import time
import json
from scrapy import signals
from pydispatch import dispatcher 

class ParariusSpider(scrapy.Spider):
    name = "pararius"
    start_urls = ['https://www.pararius.com/apartments/maastricht/page-1']
    index = 0
    results = {}


    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)


    def parse(self, response):
        path = "ul.search-list > li.search-list__item.search-list__item--listing > section.listing-search-item.listing-search-item--list.listing-search-item--for-rent> div.listing-search-item__depiction > a::attr('href')" 
        for item in response.css(path):
            time.sleep(3)
            fullitem = "https://www.pararius.com" + item.get();
            yield scrapy.Request(url = fullitem, callback = self.parsePage)

        nextPage = response.css("a.pagination__link.pagination__link--next::attr('href')").get()
        if nextPage is not None:
            time.sleep(2)
            yield scrapy.Request(url = "https://pararius.com" + nextPage, callback = self.parse)
       # else:
            #time.sleep(4)
            #yield scrapy.Request(url = "https://www.pararius.com/apartments/amsterdam", callback = self.parse)


    def parsePage(self, response):
        name = response.css("h1.listing-detail-summary__title::text").get()
        address = response.css("div.listing-detail-summary__location::text").get()
        area  = response.css("dd.listing-features__description.listing-features__description--surface_area > span::text").get()
        type_of_house = response.css("dd.listing-features__description.listing-features__description--dwelling_type > span::text").get()
        nr_of_rooms = response.css("dd.listing-features__description.listing-features__description--number_of_rooms > span::text").get()
        year_of_construction = response.css("dd.listing-features__description.listing-features__description--construction_period > span::text").get()
        nr_bedrooms = response.css("dd.listing-features__description.listing-features__description--number_of_bedrooms > span::text").get()
        nr_bathrooms = response.css("dd.listing-features__description.listing-features__description--number_of_bathrooms > span::text").get()
        price = response.css("div.listing-detail-summary__price > meta[itemprop = 'price']::attr('content')").get()

        

        self.results[self.index] = {
        "URL": response.request.url,
        "Name": name, 
        "Price": price,
        "Address": address,
        "Area (Square metre)": area,
        "Type": type_of_house,
        "Number of Rooms": nr_of_rooms,
        "Year of Construction": year_of_construction,
        "Number of Bedrooms": nr_bedrooms,
        "Number of Bathrooms": nr_bathrooms
        }


        self.index += 1

    def spider_closed(self, spider):
        with open('pararius.json', 'w') as fp:
            json.dump(self.results, fp)

